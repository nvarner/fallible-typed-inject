/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { Scope } from './api/Scope';
import { InjectionToken, INJECTOR_TOKEN, TARGET_TOKEN } from './api/InjectionToken';
import { InjectableClass, InjectableFunction, Injectable } from './api/Injectable';
import { InjectorFactory } from './api/InjectorFactory';
import { InjectionError } from './errors';
import { TChildContext } from './api/TChildContext';
import { Err, Ok, Result } from '@nvarner/monads';
import { AbstractInjector, ChildInjector, ClassProvider, FactoryProvider, RootInjector, ValueProvider } from './InjectorImpl';

const DEFAULT_SCOPE = Scope.Singleton;

/*

# Composite design pattern:

         ┏━━━━━━━━━━━━━━━━━━┓
         ┃ AbstractInjector ┃
         ┗━━━━━━━━━━━━━━━━━━┛
                   ▲
                   ┃
          ┏━━━━━━━━┻━━━━━━━━┓
          ┃                 ┃
 ┏━━━━━━━━┻━━━━━┓   ┏━━━━━━━┻━━━━━━━┓
 ┃ RootInjector ┃   ┃ ChildInjector ┃
 ┗━━━━━━━━━━━━━━┛   ┗━━━━━━━━━━━━━━━┛
                            ▲
                            ┃
          ┏━━━━━━━━━━━━━━━━━┻━┳━━━━━━━━━━━━━━━━┓
 ┏━━━━━━━━┻━━━━━━━━┓ ┏━━━━━━━━┻━━━━━━┓ ┏━━━━━━━┻━━━━━━━┓
 ┃ FactoryInjector ┃ ┃ ClassInjector ┃ ┃ ValueInjector ┃
 ┗━━━━━━━━━━━━━━━━━┛ ┗━━━━━━━━━━━━━━━┛ ┗━━━━━━━━━━━━━━━┛
*/

abstract class AbstractInjectorFactory<TContext> implements InjectorFactory<TContext> {
  private childInjectors: InjectorFactory<any>[] = [];

  public injectClass<R, Tokens extends InjectionToken<TContext>[]>(Class: InjectableClass<TContext, R, Tokens>, providedIn?: Function): R {
    try {
      const args: any[] = this.resolveParametersToInject(Class, providedIn);
      return new Class(...(args as any));
    } catch (error) {
      throw InjectionError.create(Class, error);
    }
  }

  public injectFunction<R, Tokens extends InjectionToken<TContext>[]>(fn: InjectableFunction<TContext, R, Tokens>, providedIn?: Function): R {
    try {
      const args: any[] = this.resolveParametersToInject(fn, providedIn);
      return fn(...(args as any));
    } catch (error) {
      throw InjectionError.create(fn, error);
    }
  }

  private resolveParametersToInject<Tokens extends InjectionToken<TContext>[]>(
    injectable: Injectable<TContext, any, Tokens>,
    target?: Function
  ): any[] {
    const tokens: InjectionToken<TContext>[] = (injectable as any).inject || [];
    return tokens.map((key) => {
      switch (key) {
        case TARGET_TOKEN:
          return target as any;
        case INJECTOR_TOKEN:
          return this as any;
        default:
          return this.resolveInternal(key, injectable);
      }
    });
  }

  public provideValue<Token extends string, R>(token: Token, value: R): AbstractInjectorFactory<TChildContext<TContext, R, Token>> {
    const provider = new ValueProviderFactory(this, token, value);
    this.childInjectors.push(provider as InjectorFactory<any>);
    return provider;
  }
  public provideClass<Token extends string, R, Tokens extends InjectionToken<TContext>[]>(
    token: Token,
    Class: InjectableClass<TContext, R, Tokens>,
    scope = DEFAULT_SCOPE
  ): AbstractInjectorFactory<TChildContext<TContext, R, Token>> {
    const provider = new ClassProviderFactory(this, token, scope, Class);
    this.childInjectors.push(provider as InjectorFactory<any>);
    return provider;
  }
  public provideFactory<Token extends string, R, Tokens extends InjectionToken<TContext>[]>(
    token: Token,
    factory: InjectableFunction<TContext, R, Tokens>,
    scope = DEFAULT_SCOPE
  ): AbstractInjectorFactory<TChildContext<TContext, R, Token>> {
    const provider = new FactoryProviderFactory(this, token, scope, factory);
    this.childInjectors.push(provider as InjectorFactory<any>);
    return provider;
  }
  public provideResultFactory<Token extends string, R, Tokens extends InjectionToken<TContext>[]>(
    token: Token,
    factory: InjectableFunction<TContext, Result<R, string>, Tokens>
  ): AbstractInjectorFactory<TChildContext<TContext, R, Token>> {
    const result = this.injectFunction(factory);
    const provider = result.match<ChildInjectorFactory<TContext, R, Token>>({
      ok: (value) => new ValueProviderFactory(this, token, value),
      err: (error) => new FailedInjectorFactory(this, token, error),
    });
    this.childInjectors.push(provider as InjectorFactory<any>);
    return provider;
  }

  public resolve<Token extends keyof TContext>(token: Token, target?: Function): TContext[Token] {
    return this.resolveInternal(token, target);
  }

  protected abstract resolveInternal<Token extends keyof TContext>(token: Token, target?: Function): TContext[Token];

  public abstract build(): Result<AbstractInjector<TContext>, string>;
}

class RootInjectorFactory extends AbstractInjectorFactory<{}> {
  public resolveInternal(token: never): never {
    throw new Error(`No provider found for "${token}"!.`);
  }
  public build(): Result<RootInjector, string> {
    return Ok(new RootInjector());
  }
}

abstract class ChildInjectorFactory<TParentContext, TProvided, CurrentToken extends string> extends AbstractInjectorFactory<
  TChildContext<TParentContext, TProvided, CurrentToken>
> {
  private cached: { value?: any } | undefined;

  constructor(
    protected readonly parent: AbstractInjectorFactory<TParentContext>,
    protected readonly token: CurrentToken,
    protected readonly scope: Scope
  ) {
    super();
  }

  protected abstract result(target: Function | undefined): TProvided;

  protected resolveInternal<SearchToken extends keyof TChildContext<TParentContext, TProvided, CurrentToken>>(
    token: SearchToken,
    target: Function | undefined
  ): TChildContext<TParentContext, TProvided, CurrentToken>[SearchToken] {
    if (token === this.token) {
      if (this.cached) {
        return this.cached.value;
      } else {
        try {
          const value = this.result(target);
          this.addToCacheIfNeeded(value);
          return value as any;
        } catch (error) {
          throw InjectionError.create(token, error);
        }
      }
    } else {
      return this.parent.resolve(token as any, target) as any;
    }
  }

  private addToCacheIfNeeded(value: TProvided) {
    if (this.scope === Scope.Singleton) {
      this.cached = { value };
    }
  }

  public abstract build(): Result<ChildInjector<TParentContext, TProvided, CurrentToken>, string>;
}

class FailedInjectorFactory<TParentContext, TProvided, ProvidedToken extends string> extends ChildInjectorFactory<
  TParentContext,
  TProvided,
  ProvidedToken
> {
  public constructor(parent: AbstractInjectorFactory<TParentContext>, token: ProvidedToken, private readonly errorMessage: string) {
    super(parent, token, Scope.Singleton);
  }
  protected result(): TProvided {
    throw 'Cannot get result from failed injector';
  }
  public build(): Result<ChildInjector<TParentContext, TProvided, ProvidedToken>, string> {
    return Err(`Injector failed with error: ${this.errorMessage}`);
  }

  public provideValue<Token extends string, R>(): AbstractInjectorFactory<
    TChildContext<TChildContext<TParentContext, TProvided, ProvidedToken>, R, Token>
  > {
    return (this as unknown) as AbstractInjectorFactory<TChildContext<TChildContext<TParentContext, TProvided, ProvidedToken>, R, Token>>;
  }
  public provideClass<Token extends string, R>(): AbstractInjectorFactory<
    TChildContext<TChildContext<TParentContext, TProvided, ProvidedToken>, R, Token>
  > {
    return (this as unknown) as AbstractInjectorFactory<TChildContext<TChildContext<TParentContext, TProvided, ProvidedToken>, R, Token>>;
  }
  public provideFactory<Token extends string, R>(): AbstractInjectorFactory<
    TChildContext<TChildContext<TParentContext, TProvided, ProvidedToken>, R, Token>
  > {
    return (this as unknown) as AbstractInjectorFactory<TChildContext<TChildContext<TParentContext, TProvided, ProvidedToken>, R, Token>>;
  }
  public provideResultFactory<Token extends string, R>(): AbstractInjectorFactory<
    TChildContext<TChildContext<TParentContext, TProvided, ProvidedToken>, R, Token>
  > {
    return (this as unknown) as AbstractInjectorFactory<TChildContext<TChildContext<TParentContext, TProvided, ProvidedToken>, R, Token>>;
  }
}

class ValueProviderFactory<TParentContext, TProvided, ProvidedToken extends string> extends ChildInjectorFactory<
  TParentContext,
  TProvided,
  ProvidedToken
> {
  constructor(parent: AbstractInjectorFactory<TParentContext>, token: ProvidedToken, private readonly value: TProvided) {
    super(parent, token, Scope.Transient);
  }
  protected result(): TProvided {
    return this.value;
  }
  public build(): Result<ValueProvider<TParentContext, TProvided, ProvidedToken>, string> {
    return this.parent.build().andThen((parent) => Ok(new ValueProvider(parent, this.token, this.value)));
  }
}

class FactoryProviderFactory<
  TParentContext,
  TProvided,
  ProvidedToken extends string,
  Tokens extends InjectionToken<TParentContext>[]
> extends ChildInjectorFactory<TParentContext, TProvided, ProvidedToken> {
  constructor(
    parent: AbstractInjectorFactory<TParentContext>,
    token: ProvidedToken,
    scope: Scope,
    private readonly injectable: InjectableFunction<TParentContext, TProvided, Tokens>
  ) {
    super(parent, token, scope);
  }
  protected result(target: Function): TProvided {
    return this.parent.injectFunction(this.injectable, target);
  }
  public build(): Result<FactoryProvider<TParentContext, TProvided, ProvidedToken, Tokens>, string> {
    return this.parent.build().andThen((parent) => Ok(new FactoryProvider(parent, this.token, this.scope, this.injectable)));
  }
}

class ClassProviderFactory<
  TParentContext,
  TProvided,
  ProvidedToken extends string,
  Tokens extends InjectionToken<TParentContext>[]
> extends ChildInjectorFactory<TParentContext, TProvided, ProvidedToken> {
  constructor(
    parent: AbstractInjectorFactory<TParentContext>,
    token: ProvidedToken,
    scope: Scope,
    private readonly injectable: InjectableClass<TParentContext, TProvided, Tokens>
  ) {
    super(parent, token, scope);
  }
  protected result(target: Function): TProvided {
    return this.parent.injectClass(this.injectable, target);
  }
  public build(): Result<ClassProvider<TParentContext, TProvided, ProvidedToken, Tokens>, string> {
    return this.parent.build().andThen((parent) => Ok(new ClassProvider(parent, this.token, this.scope, this.injectable)));
  }
}

export function createInjector(): InjectorFactory<{}> {
  return new RootInjectorFactory();
}
