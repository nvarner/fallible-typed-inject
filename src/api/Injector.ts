import { InjectableClass, InjectableFunction } from './Injectable';
import { InjectionToken } from './InjectionToken';

export interface Injector<TContext = {}> {
  injectClass<R, Tokens extends readonly InjectionToken<TContext>[]>(Class: InjectableClass<TContext, R, Tokens>): R;
  injectFunction<R, Tokens extends readonly InjectionToken<TContext>[]>(Class: InjectableFunction<TContext, R, Tokens>): R;
  resolve<Token extends keyof TContext>(token: Token): TContext[Token];
  dispose(): Promise<void>;
}
