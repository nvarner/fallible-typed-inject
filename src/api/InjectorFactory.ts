import { Result } from '@nvarner/monads';
import { InjectableClass, InjectableFunction } from './Injectable';
import { InjectionToken } from './InjectionToken';
import { Injector } from './Injector';
import { Scope } from './Scope';
import { TChildContext } from './TChildContext';

export interface InjectorFactory<TContext = {}> {
  provideValue<Token extends string, R>(token: Token, value: R): InjectorFactory<TChildContext<TContext, R, Token>>;
  provideClass<Token extends string, R, Tokens extends readonly InjectionToken<TContext>[]>(
    token: Token,
    Class: InjectableClass<TContext, R, Tokens>,
    scope?: Scope
  ): InjectorFactory<TChildContext<TContext, R, Token>>;
  provideFactory<Token extends string, R, Tokens extends readonly InjectionToken<TContext>[]>(
    token: Token,
    factory: InjectableFunction<TContext, R, Tokens>,
    scope?: Scope
  ): InjectorFactory<TChildContext<TContext, R, Token>>;
  provideResultFactory<Token extends string, R, Tokens extends readonly InjectionToken<TContext>[]>(
    token: Token,
    factory: InjectableFunction<TContext, Result<R, string>, Tokens>
  ): InjectorFactory<TChildContext<TContext, R, Token>>;
  build(): Result<Injector<TContext>, string>;
}
